#!/usr/bin/env python3

import sys
to_bohr = 1.88972598858

files = sys.argv[1:]
methods = [ "CC2", "CCSD", "CC3" ]

def find_text(lines,text):
    k = 0
    for line in lines:
      if line.strip().startswith(text):
        break
      k+=1
    return k

def find_empty(lines):
    k = 0
    for line in lines:
      if line.strip() == "":
        break
      k+=1
    return k

def print_energy(lines,X):
    k = find_text(lines,"Total %5s energy:"%(X.ljust(5)))
    energy = float(lines[k].split()[3]) 
    lines = lines[k:]
    k = find_text(lines,"Total energies in Hartree:")+1
    l = find_empty(lines[k:])+k
    table = [ float(line.split()[2]) for line in lines[k:l] ]
    lines = lines[l:]
    
    with open("res_%s.dat"%(X),status) as f:
      f.write("%6.2f  %15.10f  "%(distance,float(energy)))
      for x in table:
        f.write("%15.10f "%(float(x)))
      f.write("\n")

    return l



status = 'w'
for filename in files:
  with open(filename, 'r') as f:

    # Read the whole file as a list of lines
    lines = f.readlines()

    # Find geometry
    k = find_text(lines,"bond distance:")
    if(k >= len(lines)):
      distance = 0
    else:
      distance = float(lines[k].split()[6]) * to_bohr
      lines = lines[k:]

    # Find summary
    k = find_text(lines,"*                   SUMMARY OF COUPLED CLUSTER CALCULATION")
    lines = lines[k:]

    l = print_energy(lines,"CC2")
    l = print_energy(lines,"CCSD")
    l = print_energy(lines,"CC3")

    status = 'a'

