#! /bin/zsh

t=0
for j in "1 ^1A1" "2 ^1A1" "3 ^1A1" "1 ^1B1" "2 ^1B1" "3 ^1B1" "1 ^1B2" "2 ^1B2" "3 ^1B2" "1 ^1A2" "2 ^1A2" "3 ^1A2" "1 ^3A1" "2 ^3A1" "3 ^3A1" "1 ^3B1" "2 ^3B1" "3 ^3B1" "1 ^3A2" "2 ^3A2" "3 ^3A2" "1 ^3B2" "2 ^3B2" "3 ^3B2" 
do 
  t=$((t+1))
  for i in *.out
  do 
    grep $j $i | tail -1 | awk '{print $3}' >> dat_$t
  done 
done
paste -s  dat_* > res_CC3
rm dat_*
